    <div class="container">
        <div class="col-lg-12">
            <h2 class="mt--2">My Orders</h2>
            <div class="table-responsive mt--2">
                <table class="table table-striped table-bordered" id="messages-datatable" cellspacing="0" width="100%" cellpadding="10">
                    <thead>
                    <tr>
                        <th>Price ($)</th>
                        <th>Quantity</th>
                        <th>Cancel Order</th>
                        <th>Item Sold</th>
                        <th>Item Bought</th>
                        <th>Status</th>
                        <th>Date</th>
                    </tr>
                    </thead>
                    <tbody id="view_all_orders_tb"></tbody>
                </table>
            </div>
        </div>
    </div>
